import random
from tabulate import tabulate as tb
import numpy as np

def multiple(valor, multiplo):
    """
    Funcion para calcular si el numero es multiplo utilizando la division
    """
    resto = valor % multiplo
    if resto == 0:
        return True
    else:
        return False

iteracion = 200000
media, desvio = 50, 5
total_Sim = 0

#VALORES
peso_neto = 300
costo_tomate = 0.70
costo_salsa = 1.50
costos_fijos = 0.5
kg = 1000
precio_venta = 1.2

lista = []
lista_T_Sim = [] #Sumatoria de 5 tomates simulados
lista_Ganancia = [] #Lista para guardar la ganancia por envase

#Agrega el random de la distrubicion normal a la lista
for i in range(iteracion):
    lista.append(random.gauss(media, desvio))

for i in range(iteracion):
    total_Sim = total_Sim + lista[i]
    if multiple(i,5):
        peso_salsa = peso_neto - total_Sim #Peso de Salsa
        total_costo_tomate = (total_Sim * costo_tomate) / kg #Costo Tomate            
        total_costo_salsa = (peso_salsa * costo_salsa) / kg  #Costo Salsa
        costo_t_y_s = total_costo_tomate + total_costo_salsa #Costo Tomate + Salsa
        ganancia_x_envase = precio_venta - (costo_t_y_s + costos_fijos) #Ganancia por envase
        lista_T_Sim.append(total_Sim)
        lista_Ganancia.append(ganancia_x_envase)
        total_Sim = 0    

def print_EjercicioCompleto():
    """
    Funcion que muestra por pantalla los puntos D y E
    """

    total_x_envase = 0
    cont = 0

    #guarda el size de la lista para recorrer el for
    iteracion_lista = len(lista_T_Sim)
    
    for i in range(iteracion_lista):
        if lista_T_Sim[i] > 240:
           cont = cont + 1
    
    """Calcula el total por envase"""
    for i in range(iteracion_lista):
        total_x_envase = total_x_envase + lista_Ganancia[i]


    #Respuesta punto D
    print("El total de latas de tomate menor a 240gr son: ", cont)

    #Respuesta punto E
    """
    Se utiliza la funcion round para devolver el numero redondeado, y el 2do parametro es para decir 
    cuantos numeros se quieren despues de la coma
    """
    print('La ganancia media es de ', round(total_x_envase / 5, 2))