import random
import pylab

t_simulaciones = 200000
media, desvio = 200, 30
r_total = 0
lista = []
listaParteDos = []

#PRIMERA PARTE - Usando la formula vista en clase
for i in range(t_simulaciones):
    """
    Se itera y se va sumando el random con el total guardado
    """
    for j in range(12):
        r_total = r_total + random.random()
    """
    formula de distrucion normal
    """
    total = media + (desvio *(r_total - 6))
    lista.append(total)
    """
    Se resetea a cero la variable para no sumar el ultimo total 
    de r_total en la nueva suma de randoms
    """
    r_total=0

#SEGUNDA PARTE - Usando la funcion del modulo random
for i in range(t_simulaciones):
    listaParteDos.append(random.gauss(media, desvio))

#realizacion del grafico con la formula vista en clase
def print_graficoConFormula():
    pylab.figure(1)
    pylab.hist(lista)
    pylab.ylabel('Cantidad de ocurrencias')
    pylab.show()

#realizacion del grafico con la formula de random
def print_graficoConRandom():
    pylab.figure(1)
    pylab.hist(listaParteDos)
    pylab.ylabel('Cantidad de ocurrencias')
    pylab.show()

#realizacion del grafico con ambas listas de simulaciones
def print_AmbosConjuntos():
    pylab.figure(1)
    """
    Se agregan las listas de lo simulado y se los diferencia 
    con dos colores diferentes
    """
    pylab.hist(lista, color="blue")
    pylab.hist(listaParteDos, color="red")
    pylab.ylabel('Cantidad de ocurrencias')
    pylab.show()