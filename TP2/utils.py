"""
Funciones auxiliares
"""

"""
Devuelve el Tiempo de inicio de atencion
"""
def ta(tws_ant, acum):
    if tws_ant > acum:
        return tws_ant
    else:
        return acum

"""
Devuelve el array con clientes en cola
"""
def getClientesEnCola(cola):
    total = []

    for i in range(len(cola)):
        if cola[i] > 0:
            total.append(cola[i])
    
    return total

"""
Devuelve un nro redondeado en positivo
"""
def valor_dos_decimales(num):
    positivo = abs(num)
    return round(positivo, 2)