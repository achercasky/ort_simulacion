import numpy as np #Libreria para las distribuciones
import utils #Libreria auxiliar propia

media = 15 #tiempo entre llegada de clientes

a, b = 10, 15 #total de minutos para realizar un corte de pelo

cantidad_peluqueros = 1

cantidad_simulaciones = 10000

total_minutos_dia = 480 #total de minutos en un dia con jornada de 8hs

acumuladas = []

colas = [] #Clientes que se encuentren en la cola
ta = [] #Tiempo de inicio de atencion
twq = [] #Tiempo de espera en la cola
ts = [] #Tiempo de servicio
tws = [] #Tiempo final de atencion
to = [] #Tiempo ocioso
tw = [] #Tiempo en el sistema

#Agrega el random de la Distribucion exponencial a la lista
lista_llegadas = np.random.exponential(media, cantidad_simulaciones)

#Agrega el random de la Distribucion normal a la lista
lista_servicios = np.random.normal(a, b, size=cantidad_simulaciones)

#Si se encuentra un nro negrativo, se pasa a positivo
for i in range(cantidad_simulaciones):
    if lista_llegadas[i] < 0:
        lista_llegadas[i] = abs(lista_llegadas[i])
    if lista_servicios[i] < 0:
        lista_servicios[i] = abs(lista_servicios[i])


#Se van guardan las acumuladas de las Llegadas
for i in range(cantidad_simulaciones):
    if i == 0:
        valor = round(lista_llegadas[i], 2)
        acumuladas.append(valor)
    else:
        valor2 = round(acumuladas[i-1] + lista_llegadas[i], 2)
        acumuladas.append(valor2)

#Va recorriendo los diferentes tiempos de los clientes desde que entraron hasta que salieron
for i in range(cantidad_simulaciones):
    if i == 0:
        colas.append(0)
        ta.append(lista_llegadas[i])
        twq.append(0 - acumuladas[i])
        ts.append(lista_servicios[i]) #Se guarda el inicio del servicio
        tws.append(ta[i] + lista_servicios[i])
        to.append(ta[i] - 0) #Se guarda el tiempo ocioso
        tw.append(twq[i] + ts[i]) #Se guarda el tiempo en el sistem
    else:
        ta.append(utils.ta(tws[i-1], acumuladas[i]))
        twq.append(utils.valor_dos_decimales(ta[i] - acumuladas[i]))
        ts.append(lista_servicios[i]) #Se guarda el inicio del servicio
        tws.append(ta[i] + lista_servicios[i]) #
        to.append(ta[i] - tws[i-1]) #Se guarda el tiempo ocioso
        tw.append(twq[i] + ts[i]) #Se guarda el tiempo en el sistema

        if acumuladas[i] < tws[i-1]:
            colas.append(i)
        else:
            colas.append(0)

        if tws[i-1] > acumuladas[i]:
            ta.append(tws[i-1])
        else:
            ta.append(acumuladas[i])


"""
Muestra todas las respuestas
"""
def mostrar_respuestas():

    #Obtiene el ultimo valor del array
    tiempo_final_servicio = tws[-1]

    #Se guardan todos los tiempos de espera de los clientes
    waits = [abs(r) for r in twq]
    
    #Punto 1
    #Tiempo promedio del sistema --> WS --> Ultimo TWS / cantidad de clientes
    total_utilizacion_promedio_peluqueria = tiempo_final_servicio / len(lista_llegadas)
    print('1.La utilizacion promedio de la peluqueria:', round(total_utilizacion_promedio_peluqueria, 2), 'minutos')

    #Punto 2
    total_clientes_cola = sum(utils.getClientesEnCola(colas)) #Se suma todas los clientes en cola.
    #Longitud promedio de la cola --> LQ --> Total de clientes en cola / total de clientes.
    total_promedio_cola = total_clientes_cola / len(lista_llegadas)
    print('2. La cantidad promedio de clientes que esperan: ', round(total_promedio_cola, 2), ' clientes')
    
    #Punto 3
    total_suma_esperas = sum(twq) #Se suma todas las esperas
    #Tiempo promedio de espera en la cola --> WQ --> TWQ (total de tiempo de espera en la cola) / total de clientes
    average_wait = total_suma_esperas / len(lista_llegadas)
    print('3. El tiempo promedio que un cliente espera en la cola: ', utils.valor_dos_decimales(average_wait), 'minutos')