import ciw
import utils

uso_peluqueria = []
average_waits = []
longitud_promedio_cola = []
total_ocioso = []
tiempo_lavado = []
tiempo_corte = []

media = 15.0 #tiempo de llegada de clientes

a, b = 2.0, 10.0 #tiempo que dura el lavado
corte, peinado = 0.3, 0.1 #tiempo que dura el corte y peinado

cantidad_peluqueros = 2

cantidad_simulaciones = 10000

total_minutos_dia = 360 #total de minutos en un dia con jornada de 8hs

class CustomArrivalNode(ciw.ArrivalNode):
    def send_individual(self, next_node, next_individual):
        """
         Sends the next_individual to the next_node
         """
        self.number_accepted_individuals += 1
        if len(next_node.all_individuals) <= 5000:
             next_node.accept(next_individual, self.next_event_date)
        else:
            self.simulation.nodes[2].accept(next_individual, self.next_event_date)

#Se crea la instancia de las distribuciones
N = ciw.create_network(
    Arrival_distributions=[['Exponential', media], 'NoArrivals'],
    Service_distributions=[['Uniform', a, b], ['Normal', corte, peinado]],
    Transition_matrices=[[0.0, 0.0], [1.0, 0.0]],
    Number_of_servers=[1,1]
)

ciw.seed(cantidad_simulaciones)    

Q = ciw.Simulation(N, arrival_node_class=CustomArrivalNode)
Q.simulate_until_max_time(total_minutos_dia)

#Se obtienen todos los registros    
recs = Q.get_all_records()

#Obtiene el ultimo valor del array
uso_peluqueria.append(recs[-1].service_end_date / len(recs))

#Se guardan todos los tiempos de espera de los clientes
waits = [r.waiting_time for r in recs]
average_waits.append(sum(waits) / len(waits))

#Se guardan todos los clientes que hayan estado en la cola
clientes_en_cola = [r.queue_size_at_arrival for r in recs if r.queue_size_at_arrival >= 1]
if not clientes_en_cola:
    clientes_en_cola.append(0)
else:
    longitud_promedio_cola.append(sum(clientes_en_cola) / len(clientes_en_cola))

#Se guarda el tiempo ocioso (si hubo)
tiempo_ocioso = [r.time_blocked for r in recs if r.time_blocked > 0]
if not tiempo_ocioso:
    total_ocioso.append(0)
else:
    total_ocioso.append(sum(tiempo_ocioso) / len(tiempo_ocioso))

#Total de lavados que terminaron
lavado = [r.service_end_date for r in recs if r.node==1 and r.customer_class==0]
tiempo_lavado.append(sum(lavado) / len(lavado))

corte_y_peinado = [r.service_end_date for r in recs if r.node==2 and r.customer_class==0]
if not corte_y_peinado:
    tiempo_corte.append(0)
else:
    tiempo_corte.append(sum(corte_y_peinado) / len(corte_y_peinado))
    
"""
Muestra todas las respuestas
"""
def print_informe():

    #Longitud promedio de la cola --> LQ --> Total de clientes en cola / total de clientes.
    total_promedio_cola = sum(longitud_promedio_cola) / len(longitud_promedio_cola)
    print('a) La longitud promedio de las dos colas: ', round(total_promedio_cola, 2), ' clientes')
    
    #Tiempo promedio de espera en la cola --> WQ --> TWQ (total de tiempo de espera en la cola) / total de clientes
    average_wait = sum(average_waits) / len(average_waits)
    print('b) El tiempo promedio que tardan los clientes en ser atendidos: ', round(average_wait, 2), 'minutos')
    
    #Tiempo promedio del sistema --> WS --> Ultimo TWS / cantidad de clientes
    total_utilizacion_promedio_peluqueria = sum(uso_peluqueria) / len(uso_peluqueria)
    total_lavado = sum(tiempo_lavado) / len(tiempo_lavado)
    total_corte = sum(tiempo_corte) / len(tiempo_corte)
    print('c) Tiempo medio de los clientes en el Sistema: ', 
        round(total_utilizacion_promedio_peluqueria, 2), 'minutos y en cada uno de los servicios: ')
    print('Lavado: ', round(total_lavado, 2))
    print('Corte y Peinado: ', utils.valor_dos_decimales(total_corte))
    
    #Tiempo ocioso (%) --> To / TWS (Ultimo) 
    total_tiempo_ocioso = sum(total_ocioso) / len(total_ocioso)
    to = total_tiempo_ocioso / sum(uso_peluqueria)
    print('d) Porcentaje del tiempo ocioso de cada uno de los empleados: ', to, '%' )
    
    